<?php
    $cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php include('header.php'); ?>

<main class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Hello Dev Team</h1>
            <div class="fader"></div>
        </div>
    </div>
</main>

<?php include('footer.php'); ?>
