<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='<?php echo $cMetaDesc; ?>' name='description'>
    <meta content='<?php echo $cMetaKW; ?>' name='keywords'>
    <title></title>
    <link href="stylesheets/styles.css" rel="stylesheet">
    <script src="_/js/modernizr.custom.55102.js"></script>
    <script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXX-Y']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
    </script>
</head>
<body class="<?=$layout ?>">
<header></header>
